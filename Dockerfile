FROM ubuntu:22.04

RUN apt-get update -y
RUN apt install -y build-essential

RUN apt-get install -y rsync

RUN apt-get install -y git
RUN apt-get install -y make
RUN apt-get install -y bc

# Install MPI
RUN apt-get install -y mpich 

# Install Python and libraries
RUN apt-get install -y python3 python3-pip  
RUN pip3 install numpy

RUN echo "Versions==="
RUN python3 -v
RUN mpirun --version
RUN pip3 freeze